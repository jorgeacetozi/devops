package com.devops.api;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import com.devops.model.Curso;

@RestController
public class DevopsController {

	@RequestMapping(value = "/api", method = RequestMethod.GET, produces = "application/json")
	public Curso home(){
		return new Curso("AWS", "Usando a nuvem da Amazon");
	}
	@RequestMapping(value = "/new", method = RequestMethod.GET, produces = "application/json")
	public Curso newFeature(){
		return new Curso("CanaryRelease", "Nova Funcionalidade");
	}
	@RequestMapping(value = "/xuxa", method = RequestMethod.GET, produces = "application/json")
	public Curso newFeatureXuxa(){
		return new Curso("Xuxa", "Xuxa Meneguel");
	}
}
