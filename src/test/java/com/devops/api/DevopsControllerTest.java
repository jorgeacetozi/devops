package com.devops.api;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit4.SpringRunner;

import com.devops.model.Curso;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class DevopsControllerTest {
	
	@Autowired
	private TestRestTemplate template;

	@Test
	public void shouldGetAwsCourse() {
		ResponseEntity<Curso> response = template.getForEntity("/api", Curso.class);
		Curso curso = response.getBody();
		assertThat(curso.getNome(), equalTo("AWS"));
		assertThat(curso.getDescricao(), equalTo("Usando a nuvem da Amazon"));
	}

	@Test
	public void shouldGetXuxaMeneguel() {
		ResponseEntity<Curso> response = template.getForEntity("/xuxa", Curso.class);
		Curso curso = response.getBody();
		assertThat(curso.getNome(), equalTo("Xuxa"));
		assertThat(curso.getDescricao(), equalTo("Xuxa Meneguel"));
	}
}
